import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:flutter/services.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:fk_user_agent/fk_user_agent.dart';

class WebViewComponent extends StatefulWidget {
  final String url;

  const WebViewComponent({super.key, required this.url});

  @override
  _WebViewComponentState createState() => _WebViewComponentState();
}

class _WebViewComponentState extends State<WebViewComponent> {
  late InAppWebViewController _controller;
  final GlobalKey webViewKey = GlobalKey();
  String _userAgent = 'Unknown';
  bool _isUserAgentSet = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) async {
      await FkUserAgent.init();
      await initUserAgent();
    });
    _requestPermissions();
  }

  Future<void> _requestPermissions() async {
    var status = await Permission.camera.status;
    if (!status.isGranted) {
      await Permission.camera.request();
    }
  }

  Future<void> initUserAgent() async {
    try {
      String? userAgent = FkUserAgent.webViewUserAgent.toString();
      setState(() {
        _userAgent = userAgent;
        _isUserAgentSet = true;
      });
    } on PlatformException catch (e) {
      print('Failed to get platform version: $e');
      setState(() {
        _userAgent = 'Failed to get platform version.';
        _isUserAgentSet = true;
      });
    }
  }

  Future<void> _handleBack(BuildContext context) async {
    var canGoBack = await _controller.canGoBack();
    if (canGoBack) {
      _controller.goBack();
    } else {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Você deseja sair do Aprimora?'),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text('Não'),
            ),
            TextButton(
              onPressed: () {
                SystemNavigator.pop();
              },
              child: Text('Sim'),
            ),
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        await _handleBack(context);
        return false;
      },
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(0.0),
          child: AppBar(
            centerTitle: true,
          ),
        ),
        body: _isUserAgentSet
            ? InAppWebView(
                key: webViewKey,
                initialUrlRequest: URLRequest(url: Uri.parse(widget.url)),
                initialOptions: InAppWebViewGroupOptions(
                  crossPlatform: InAppWebViewOptions(
                    userAgent: _userAgent,
                    applicationNameForUserAgent: "Aprimora",
                    javaScriptEnabled: true,
                    useShouldOverrideUrlLoading: true,
                    useOnLoadResource: true,
                    cacheEnabled: true,
                  ),
                ),
                onWebViewCreated: (controller) {
                  _controller = controller;
                },
                androidOnPermissionRequest: (controller, origin, resources) async {
                  if (resources.contains('android.webkit.resource.VIDEO_CAPTURE')) {
                    var status = await Permission.camera.status;
                    if (!status.isGranted) {
                      await Permission.camera.request();
                    }
                  }
                  return PermissionRequestResponse(
                    resources: resources,
                    action: PermissionRequestResponseAction.GRANT,
                  );
                },
              )
            : Center(child: CircularProgressIndicator()),
      ),
    );
  }
}

Future<void> main() async {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  await Permission.camera.request(); // Solicita permissão de câmera na inicialização
  runApp(const MaterialApp(
    home: Scaffold(
      body: WebViewComponent(
        url: "https://hub.educacional.com/sso?applicationClientId=aprimoraweb.educacional.com.br",
      ),
    ),
  ));
  await Future.delayed(const Duration(milliseconds: 1500), () {});
  FlutterNativeSplash.remove();
}
